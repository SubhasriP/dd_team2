package com.dollardays.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class AccountOverviewPage {  
	
	WebDriver driver;
	
	public AccountOverviewPage(WebDriver driver) {  
		
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/header/div/div/div/div[3]/div/ul/li[1]/a/img")
private WebElement signIndropdown;

public WebElement getsignIndropdown() {
return signIndropdown;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/header/div/div/div/div[3]/div/ul/li[1]/ul/li[2]/a")
private WebElement Accounts;
public WebElement getAccounts() {
	return Accounts;
	
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[1]/div/h5")
private WebElement logininformation;

public  WebElement getLoginInformation() {
	return logininformation;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[1]/div/h6[1]")
private WebElement emailAddress;

public WebElement getEmailAddress() {
	return emailAddress;
}

@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[1]/div/h6[2]")
private WebElement password;

public WebElement getPassword() {
	return password;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[1]/div/h5/a")
private WebElement editlink;

public WebElement getEditbtn() {
	return editlink;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div/h5")
private WebElement personalInformation;
public WebElement getPersonalinfo() {
	return personalInformation;
}	
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/h6[1]")
private WebElement Name;
public WebElement name() {
	return Name;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/h6[2]")
	private WebElement primaryNumber;
public WebElement getPrimaryNumber() {
return primaryNumber;
}	

@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[1]/div/span[1]")
private WebElement loginName;
public WebElement getLoginName() {
return loginName;
}	



@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div/div/div[1]/h6[3]")
private WebElement secondaryNumber;
public WebElement getSecondaryNumber() {
	return secondaryNumber;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[1]/div[2]/div/h5/a")
private WebElement Editbtn1;
public WebElement getEditbtn1() {
	return Editbtn1;
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[2]/div[1]/div/h5")
	private WebElement DefaultShipToAddress;
public WebElement getDefaultShipToAddress() {
	return DefaultShipToAddress;
	
}
@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[5]/section/div/div[3]/div/div[2]/div/div[2]/div[1]/div/h5/a")	
	private WebElement Editbtn2;
public WebElement getEditbtn2() {
	return Editbtn2;
	
}
@FindBy(xpath="//*[@id='aspnetForm']/div[5]/section/div/div[3]/div/div[2]/div/div[2]/div[2]/div/h5")
private WebElement DefaultPaymentMethod;
public WebElement getDefaultPaymentMethod(){
	return DefaultPaymentMethod;
	
}
	
@FindBy(xpath="//*[@href='wallet.aspx']")	
private WebElement Editbtn3; 
public WebElement getEditbtn3() {
	return Editbtn3;
	
}
@FindBy(xpath="//*[@id=\"taxexemptdiv\"]/h5")	
private WebElement CustomerType;
public WebElement getCustomerType() {
	return CustomerType;
	
}
@FindBy(xpath="//*[@id=\"taxexemptdiv\"]/div/div[1]/h6")
private WebElement OrganizationType;
public WebElement getOrganizationType() {
	return OrganizationType;
	
}
@FindBy(xpath="//*[@id=\"taxexemptdiv\"]/div/div[2]/h6")
private WebElement SalesTaxID;
public WebElement getSalesTaxID() {
	return SalesTaxID;
	
}
@FindBy(xpath="//*[@id=\"taxexemptdiv\"]/h5/a")
private WebElement Editbtn4;
public WebElement getEditbtn4() {
	return Editbtn4;
	
}
public void validateLoginInformation(String userName,String password) throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	boolean isLogininfo=getLoginInformation().isDisplayed();
	String loginName=getLoginName().getText();
	if(isLogininfo) {
		Assert.assertEquals(userName, loginName);
		
	}

}

public void validatePersonalInformation() throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	//boolean personalInfo= getPersonalinfo().isDisplayed();
	String personalName=getPersonalinfo().getText();
	//String actualPersonalName="PERSONAL INFORMATION\r\n"
		//	+ "Edit";
	//Assert.assertEquals(actualPersonalName,
			// personalName);
	
	String primary = getPrimaryNumber().getText();
	Assert.assertEquals("Primary #:" , primary); 
String second =	getSecondaryNumber().getText();
Assert.assertEquals("Secondary #:" ,second); 
	getEditbtn1().click();	
}
public void defaultShipToAddress() throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	getDefaultShipToAddress().isDisplayed();
	getEditbtn2().click();	
}
public void defaultPaymentMethod() throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	getDefaultPaymentMethod().isDisplayed();
	
}
public void customerType() throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	getCustomerType().isDisplayed();
	getOrganizationType().isDisplayed();
	getSalesTaxID().isDisplayed();
	getEditbtn4().click();
	
	
}



public void accountOverview() throws InterruptedException {
	Thread.sleep(1000);
	getsignIndropdown().click();
	Thread.sleep(1000);
	getAccounts().click();
	//getLoginInformation().isDisplayed();
	getEmailAddress().isDisplayed();
	getPassword().isDisplayed();
	getEditbtn().click();
	Thread.sleep(1000);
	getPersonalinfo().isDisplayed();
	getPrimaryNumber().isDisplayed();
	getSecondaryNumber().isDisplayed();
	getEditbtn1().click();
	Thread.sleep(1000);


	Thread.sleep(1000);
	getDefaultPaymentMethod().isDisplayed();
	//getEditbtn3().click();
	Thread.sleep(1000);
	getCustomerType().isDisplayed();
	getOrganizationType().isDisplayed();
	getSalesTaxID().isDisplayed();
	getEditbtn4().click();
	
	
	
}

































}
















