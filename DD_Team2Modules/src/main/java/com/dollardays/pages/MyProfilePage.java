package com.dollardays.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyProfilePage {

//Home Page Locators

@FindBy(xpath="//a[@class='dropdown-toggle']//img[@class='header-user']")
//@FindBy(xpath = "//*[@id=\"aspnetForm\"]/header/div[2]/div/div/div[3]/div/ul/li[1]/a/img")



private WebElement User_Dropdown_Toggle;

@FindBy(xpath="//*[@id=\"aspnetForm\"]/header/div[2]/div/div/div[3]/div/ul/li[1]/a/img")
private WebElement userDropdownIcon;


public void clickDropDown() {
userDropdownIcon.click();
}

@FindBy(xpath="//a[@href='/logout.aspx'][@class='dropdown-item padditem margn-top']")
private WebElement User_Dropdown_Toggle_Signout;

@FindBy(xpath="//a[@class='dropdown-item'][@href='/myaccount/account.aspx']")
private WebElement User_Dropdown_Toggle_Accounts;

@FindBy(xpath="//a[@class='dropdown-item'][@href='/myaccount/profile.aspx']")
private WebElement User_Dropdown_Toggle_Profile;

     
//My Profile Link in side menu Locators

@FindBy(xpath = "//span[text()='My Profile']")
private WebElement profileLink;


   
//Email Preferences Block Locators

//@FindBy(xpath="//h5[text()='Manage the types of emails sent to ']/span")
@FindBy(xpath="//*[@id=\"ctl00_cphContent_pnlPassword\"]/h4/span")

public WebElement Email_Username;

@FindBy(xpath="//input[@name='ctl00$cphContent$txtCurrentPassword']")
public WebElement profile_currentpassword_text;
public WebElement getProfilecurrentPassword() {
return profile_currentpassword_text;
}
public void feedPassword(String password) {
getProfilecurrentPassword().sendKeys(password);
}

@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtNewPassword\"]")
public WebElement profile_new_password_text;
public WebElement getProfileNewPassword() {
return profile_new_password_text;
}
public void feedNewPassword(String password) {
getProfileNewPassword().sendKeys(password);
}

@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtRepeatedPassword\"]")
public WebElement profile_retype_new_password_text;
public WebElement getProfile_Retype_NewPassword() {
return profile_retype_new_password_text;
}
public void feedretypeNewPassword(String password) {
getProfile_Retype_NewPassword().sendKeys(password);
}

@FindBy(xpath ="//*[@id=\"ctl00_cphContent_btnUpdatePassword\"]")

private WebElement savechanges;


public void clickSaveChanges() {
savechanges.click();
}


@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtFName\"]") 
public WebElement profile_firstname;
public WebElement getProfile_firstname() {
return profile_firstname;

}
public void feedFirstname(String firstname) {
getProfile_firstname().clear();
getProfile_firstname().sendKeys(firstname);
}

@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtLName\"]") 
public WebElement profile_Lastname;
public WebElement getProfile_Lastname() {
return profile_Lastname;

}
public void feedLastname(String Lastname) {
getProfile_Lastname().clear();
getProfile_Lastname().sendKeys(Lastname);
}


@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtPhoneMain\"]") 
public WebElement profile_PrimaryPhoneNo;
public WebElement getProfile_PrimaryPhoneNo() {
return profile_PrimaryPhoneNo;

}
public void feedPrimaryPhoneNo(String PrimaryPhoneNo) {
System.out.println("Coming inside feeprimary");
getProfile_PrimaryPhoneNo().clear();
getProfile_PrimaryPhoneNo().sendKeys(PrimaryPhoneNo);
}

@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtPhoneMainExt\"]") 
public WebElement profile_Ext;
public WebElement getProfile_Ext() {
return profile_Ext;

}
public void feedExt(String Ext) {
getProfile_Ext().clear();
getProfile_Ext().sendKeys(Ext);
}

@FindBy(xpath="//*[@id=\"ctl00_cphContent_txtPhoneBill\"]") 
public WebElement profile_Secondary_Mobilephoneno;
public WebElement getProfile_SecondaryMobilePhoneno()
{
return profile_Secondary_Mobilephoneno;

}
public void feedSecondaryPhoneNo(String SecondaryMobilePhoneno) {
getProfile_SecondaryMobilePhoneno().clear();
getProfile_SecondaryMobilePhoneno().sendKeys(SecondaryMobilePhoneno);
}

@FindBy(xpath = "//*[@id=\"ctl00_cphContent_btnUpdatePersonalInfo\"]")
private WebElement savePersonalchanges;


public void clickSavePersonalChanges() {
savePersonalchanges.click();
}



public void click_User_Dropdown_Toggle_Profile() {
User_Dropdown_Toggle_Profile.click();
}

public MyProfilePage(WebDriver driver) {
PageFactory.initElements(driver, this);
}
}
