package com.dollardays.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.dollardays.listners.ExtentTestManager;

public class SearchAndRelatedPages {
	
		WebDriver driver;
		
		public SearchAndRelatedPages(WebDriver driver) {
			this.driver = driver;
			PageFactory.initElements(driver, this);
		}
		
		@FindBy(xpath="//input[@type='text'and @name='terms']")
		private WebElement searchBar;
		
		public WebElement getSearchBar() {
			return searchBar;
		}
		
		@FindBy(xpath="//i[@class='fa fa-search']")
		private WebElement searchBtn;
		
		public WebElement getSearchBtn() {
			return searchBtn;
		}
		
		@FindBy(xpath="//*[@id='facetrefinements']/aside[1]/div/h3/span[@class='sku-count']")
		private WebElement searchCount;
		
		public WebElement getSearchCount() {
			return searchCount;
		}
		
		@FindBy(xpath="//*[@class='failed-search-results bd']")
		private WebElement noDataFound;
		
		public WebElement getnoDataFoundMsg() {
			return noDataFound;
		}
		
		@FindAll(@FindBy(xpath="//div[@class='select-bar pagination-bar']//a[contains(@class,'page-link')]"))
		private List<WebElement> pageCount;
		
		public List<WebElement> getPageCount(){
			return pageCount;
		}
		
		@FindBy(xpath="//a[@title='Next Page']")
		private WebElement nextBtn;
		
		public WebElement getNextPageBtn() {
		return nextBtn;
		}
		////a[@title='LastPage']"}
		@FindBy(xpath="//*[@id=\"aspnetForm\"]/div[6]/div[2]/div[1]/div/div/div[2]/div/div/div[2]/div[2]/div/ul/li[7]/a/i")
		private WebElement lastPageBtn;
		
		public WebElement getLastPageBtn() {
			return lastPageBtn;
		}
		
		@FindBy(xpath="//a[@title='First Page']")
		private WebElement firstPageBtn;

		public WebElement getFirstPageBtn() {
			return firstPageBtn;
		}

		@FindBy(xpath="//li[@class='active page-item']")
		private WebElement lastBtnText;
		
		public WebElement getLastBtnText() {
			return lastBtnText;
		}
		
		@FindAll(@FindBy(xpath = "//div[contains(@class,'prod-tile')]"))
		private List<WebElement> pageItemsCount;

		public List<WebElement> getPageItemsCount() {
			return pageItemsCount;
		}
		
		public String getSearchWithValue() throws InterruptedException
		{
			String msg = null;
			String result = getnoDataFoundMsg().getText();
			System.out.println("-->"+result);
			
			if(result.contains("No Results Found")) {
				msg = getnoDataFoundMsg().getText();
			}else {
				msg = getSearchCount().getText();
			}
			return msg;
		}
		
		/*public void getSearchWithInvalidData()
		{
			if(driver.getPageSource().contains("No Result Found")) {
				System.out.println("No Result Found - Invalid data");
				Assert.assertTrue(true);
			}
		}--by abirami*/
		
		public void getItemCount() throws InterruptedException {
			getLastPageBtn().click();
			Thread.sleep(1000);
			String valueCount = getLastBtnText().getText();
			getFirstPageBtn().click();
			Thread.sleep(1000);
			
			for(int i=1;i<Integer.parseInt(valueCount);i++) {
			    getNextPageBtn().click();
				Thread.sleep(1000);
				System.out.println("Page "+ i + "  contains " + getPageItemsCount().size() + " rows");
				ExtentTestManager.getTest().log(Status.PASS, "Page Number "+ i + "  contains " + getPageItemsCount().size() + " rows");
			    Thread.sleep(1000);
			}
		}

}