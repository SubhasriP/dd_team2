package com.dollardays.testcases;

import java.util.Hashtable;

import org.testng.annotations.Test;

import com.dollardays.commons.Base64;
import com.dollardays.pages.AccountOverviewPage;
import com.dollardays.pages.LoginPage;
import com.dollardays.utilities.DDDataProvider;
import com.dollardays.utilities.TestUtil;

public class AccountOverviewTestcases extends BaseTest{
	
	
	
	/*@DDDataProvider(datafile = "testdata/testdata1.xlsx", sheetName = "SearchBar",  testcaseID = "TC1", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	public void invokeAccountOverview(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		//ExtentTestManager.getTest().log(Status.INFO, "login tstcase");
		//Thread.sleep(1000);
		System.out.println("Insidr invokeLogin");
		LoginPage loginPage = new LoginPage(driver);
		
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));	
		AccountOverviewPage accountOverviewpage=new AccountOverviewPage(driver);
		accountOverviewpage.accountOverview();
		//VideoRecorder_utlity.stopRecord();//End point of video recording
		
	}*/

	
	
	@DDDataProvider(datafile = "testdata/testdata1.xlsx", sheetName = "Sheet1",  testcaseID = "", runmode = "Yes") 
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
	public void validateAccountOverviewLoginInformation(Hashtable<String, String> datatable) throws Exception{
		//VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
		//ExtentTestManager.getTest().log(Status.INFO, "login tstcase");
		String username=datatable.get("Username");
		System.out.println("username"+username);
		
		String password=Base64.decrypt(datatable.get("Password"));
		System.out.println("username"+password);


		LoginPage loginPage = new LoginPage(driver);
		
		loginPage.login(username,password);
		AccountOverviewPage accountOverviewpage=new AccountOverviewPage(driver);
		accountOverviewpage.validateLoginInformation(username,password);
		accountOverviewpage.validatePersonalInformation();
		accountOverviewpage.defaultShipToAddress();
		accountOverviewpage.defaultPaymentMethod();
		accountOverviewpage.customerType();
		Thread.sleep(1000); 
		System.out.println(datatable.get("TestCases"));
	}
		
}
