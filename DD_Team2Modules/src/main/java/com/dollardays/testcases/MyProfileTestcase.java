package com.dollardays.testcases;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Hashtable;
import java.util.Map;

import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.dollardays.commons.Base64;
import com.dollardays.listners.ExtentTestManager;
import com.dollardays.pages.LoginPage;
import com.dollardays.utilities.DDDataProvider;
import com.dollardays.utilities.JsonReader;
import com.dollardays.utilities.TestUtil;
import com.dollardays.utilities.VideoRecorder_utlity;
import com.dollardays.pages.*;

public class MyProfileTestcase extends BaseTest{

@DDDataProvider(datafile = "testdata/MyProfile.xlsx", sheetName = "My Profile",  testcaseID = "TC1", runmode = "Yes")
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
public void Login_PasswordUpdate(Hashtable<String, String> datatable) throws Exception{
VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
//ExtentTestManager.getTest().log(Status.INFO, "login tstcase");
Thread.sleep(1000);
LoginPage loginPage = new LoginPage(driver);
loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password"))); 
//Thread.sleep(10000);
MyProfilePage myprofile = new MyProfilePage(driver);
//myprofile.click_User_Dropdown_Toggle();
myprofile.clickDropDown();
myprofile.click_User_Dropdown_Toggle_Profile();
myprofile.feedPassword(Base64.decrypt(datatable.get("Password")));
myprofile.feedNewPassword(Base64.decrypt(datatable.get("New Password")));
myprofile.feedretypeNewPassword(Base64.decrypt(datatable.get("New Password")));

//myprofile.clickSaveChanges();


Thread.sleep(10000);
VideoRecorder_utlity.stopRecord();//End point of video recording

}

@DDDataProvider(datafile = "testdata/MyProfile.xlsx", sheetName = "My Profile",  testcaseID = "TC2", runmode = "Yes")
@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
public void personalInfoUpdate(Hashtable<String, String> datatable) throws Exception{
VideoRecorder_utlity.startRecord("GoogleTestRecording");//Starting point of video recording
//ExtentTestManager.getTest().log(Status.INFO, "login tstcase");
Thread.sleep(1000);
LoginPage loginPage = new LoginPage(driver);
loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password"))); 

MyProfilePage myprofile = new MyProfilePage(driver);
//myprofile.click_User_Dropdown_Toggle();
myprofile.clickDropDown();
myprofile.click_User_Dropdown_Toggle_Profile();
myprofile.feedFirstname(datatable.get("Firstname"));
myprofile.feedLastname(datatable.get("LastName"));
myprofile.feedPrimaryPhoneNo(datatable.get("PrimaryPhoneNo"));
myprofile.feedExt(datatable.get("Ext"));
myprofile.feedSecondaryPhoneNo(datatable.get("SecondaryMobilePhoneno"));
//myprofile.clickSavePersonalChanges();
//myprofile.clickSaveChanges();


Thread.sleep(10000);
VideoRecorder_utlity.stopRecord();//End point of video recording

}

}

