package com.dollardays.testcases;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Hashtable;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.dollardays.commons.Base64;
import com.dollardays.listners.ExtentTestManager;
import com.dollardays.pages.LoginPage;
import com.dollardays.pages.SearchAndRelatedPages;
import com.dollardays.utilities.DDDataProvider;
import com.dollardays.utilities.TestUtil;

public class SearchAndRelatedPagesTestcase extends BaseTest{
	
	@DDDataProvider(datafile = "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search",testcaseID = "TC1", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
    public void TC_01_searchPageWithNavigationLinks(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException
	{
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 3 : Validate Product Navigation Pages ");
		LoginPage loginPage = new LoginPage(driver);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
		
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
		
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
		try {
			
			snrpage.getItemCount();
			Thread.sleep(10000);
			
		}catch (Exception e) {
			snrpage.getItemCount();
			Thread.sleep(10000);
		}

	}
	
	@DDDataProvider(datafile= "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search", testcaseID = "TC2", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
		
	public void TC_02_searchWithCaseSensitiveData(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException{
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 1 : Verify Search functionality");
		LoginPage loginPage = new LoginPage(driver);
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
				
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
				
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
				
		String categoryCount = snrpage.getSearchCount().getText();
		ExtentTestManager.getTest().log(Status.PASS, "Step 4  : Should display '"+categoryCount+"'");
				
		Thread.sleep(500);
		loginPage.getUserDrodown().click();
		Thread.sleep(500);
		loginPage.getLogoutBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 5  : Clicked on LogOut");
			}
	
	
	@DDDataProvider(datafile= "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search", testcaseID = "TC3", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
		
	public void TC_03_searchWithSpecialCharacters(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException{
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 1 : Verify Search functionality");
		LoginPage loginPage = new LoginPage(driver);
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
				
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
				
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
				
		String categoryCount = snrpage.getSearchCount().getText();
		ExtentTestManager.getTest().log(Status.PASS, "Step 4  : Should display '"+categoryCount+"'");
				
		Thread.sleep(500);
		loginPage.getUserDrodown().click();
		Thread.sleep(500);
		loginPage.getLogoutBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 5  : Clicked on LogOut");
		}

	
	@DDDataProvider(datafile= "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search", testcaseID = "TC4", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
		
	public void TC_04_searchWithSKUNumber(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException{
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 1 : Verify Search functionality");
		LoginPage loginPage = new LoginPage(driver);
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
				
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
				
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
				
		String categoryCount = snrpage.getSearchCount().getText();
		ExtentTestManager.getTest().log(Status.PASS, "Step 4  : Should display '"+categoryCount+"'");
				
		Thread.sleep(500);
		loginPage.getUserDrodown().click();
		Thread.sleep(500);
		loginPage.getLogoutBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 5  : Clicked on LogOut");
		}
	
	@DDDataProvider(datafile= "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search", testcaseID = "TC5", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
		
	public void TC_05_searchWithInValidData(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException{
		
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 1 : Verify Search functionality");
		LoginPage loginPage = new LoginPage(driver);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
		
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
				
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
		
		String nodatafound = snrpage.getnoDataFoundMsg().getText();
		ExtentTestManager.getTest().log(Status.PASS, "Step 4  : Should display '"+nodatafound+"'");
		
		//snrpage.getSearchWithInvalidData();--by abhi
		
		Thread.sleep(500);
		loginPage.getUserDrodown().click();
		Thread.sleep(500);
		loginPage.getLogoutBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 5  : Clicked on LogOut");
			}
	
	
	@DDDataProvider(datafile= "testdata/SearchAndRelatedPages.xlsx", sheetName = "Search", testcaseID = "TC6", runmode = "Yes")
	@Test(dataProvider = "dd-dataprovider", dataProviderClass = TestUtil.class)
		
	public void TC_06_searchWithEdgeCases(Hashtable<String, String> datatable) throws InterruptedException, UnsupportedEncodingException, GeneralSecurityException
	{
		
		ExtentTestManager.getTest().log(Status.PASS, "Testcase 1 : Verify Search functionality");
		LoginPage loginPage = new LoginPage(driver);
		ExtentTestManager.getTest().log(Status.PASS, "Step 1  : Login with Valid credentials");
		loginPage.login(datatable.get("UserName"), Base64.decrypt(datatable.get("Password")));
		Thread.sleep(1000);
		SearchAndRelatedPages snrpage = new SearchAndRelatedPages(driver);
				
		snrpage.getSearchBar().sendKeys(datatable.get("Item").trim());
		ExtentTestManager.getTest().log(Status.PASS, "Step 2  : Enter search data in the search bar");
				
		snrpage.getSearchBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 3  : clicked on the search button");
		Thread.sleep(500);
				
		String categoryCount = snrpage.getSearchCount().getText();
		ExtentTestManager.getTest().log(Status.PASS, "Step 4  : Should display '"+categoryCount+"'");
				
		Thread.sleep(500);
		loginPage.getUserDrodown().click();
		Thread.sleep(500);
		loginPage.getLogoutBtn().click();
		ExtentTestManager.getTest().log(Status.PASS, "Step 5  : Clicked on LogOut");
		}
	
}
